import React from 'react';
import ReactDOM from 'react-dom';
import App from './Containers/Home';
import Carrinho from './Containers/Carrinho';
import { BrowserRouter as Router, Route } from "react-router-dom";

ReactDOM.render(
<Router>
      <div>
        <Route exact path="/" component={App} />
         <Route path="/carrinho" component={Carrinho} />
      </div>
    </Router>
    ,
document.getElementById('root')
);