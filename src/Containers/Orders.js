/* eslint-disable no-script-url */

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Title from "../Title";
import products from "../Components/Produtos";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
// Generate Order Data


const useStyles = makeStyles(theme => ({
  seeMore: {
    marginTop: theme.spacing(3)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
}));

const formatter = new Intl.NumberFormat("pt-BR", {
  style: "currency",
  currency: "BRL",
  minimumFractionDigits: 2
});

const addProduct = id => {
  var idProduto = id.toString();

  var getStoreProducts = localStorage.getItem("produtos");

  var names = [];
  names = JSON.parse(getStoreProducts);

  names.push(idProduto);

  getStoreProducts = JSON.stringify(names);

  localStorage.setItem("produtos", getStoreProducts);

  window.location.reload();
};

const removeProduct = id => {
  var idProduto = id.toString();
  var getStoreProducts = localStorage.getItem("produtos");

  var names = [];
  names = JSON.parse(getStoreProducts);

  names.splice(names.indexOf(idProduto), 1);

  getStoreProducts = JSON.stringify(names);

  localStorage.setItem("produtos", getStoreProducts);

  window.location.reload();
};

const removeProductAll = id => {
  var getStoreProducts = localStorage.getItem("produtos");

  var names = [];
  names = JSON.parse(getStoreProducts);

  for (var i = 0; i < names.length; i++) {
    if (names[i] === id.toString()) {
      names.splice(i, 1);
      i--;
    }
  }

  getStoreProducts = JSON.stringify(names);

  localStorage.setItem("produtos", getStoreProducts);

  alert("Produto Removido!");

  window.location.reload();
};

export default function Orders() {
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  let produtos = JSON.parse(localStorage.getItem("produtos"));

  //realiza ordenação
  if(produtos){
  produtos = produtos.sort(function(a, b) {
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    return 0;
  });
}
  var array_ids_produtos = [];
  var actual_id = 0;
  var qtde = 1;
  var estoque = 0;
  let valor = 0;
  var chave = 0;
  var person;

  if (produtos && produtos.length > 0) {
    produtos.forEach(function(element) {
      products.forEach(function(value, key) {
        if (element == value.id) {
          if (actual_id != value.id) {
            console.log("cima", value.id);
            qtde = 1;
            estoque = value.quantity - qtde;
            valor = valor + (value.price * qtde);
            if (estoque >= 0) {
              person = {
                quantity: estoque,
                price: value.price,
                title: value.title,
                id: value.id,
                qtde: qtde
              };

              array_ids_produtos[key] = person;
            }
            chave = key;
            actual_id = value.id;
          } else {
            qtde = qtde + 1;
            estoque = value.quantity - qtde;
            valor = valor + (value.price * qtde);
            if (estoque >= 0) {
              person = {
                quantity: estoque,
                price: value.price,
                title: value.title,
                id: value.id,
                qtde: qtde
              };
              array_ids_produtos[chave] = person;
            }
            actual_id = value.id;
          }
        }
      });
    });
  }
  
  //Correção no calculo
  if(array_ids_produtos.length == 0){
    valor = '0.00';

    var getStoreProducts = localStorage.getItem("produtos");

    var names = [];
    names = JSON.parse(getStoreProducts);

    for (var i = 0; i < names.length; i++) {
    
        names.splice(i, 1);
        i--;
    
    }

    getStoreProducts = JSON.stringify(names);

    localStorage.setItem("produtos", getStoreProducts);

  }
  return (
    <React.Fragment>
      <Grid container spacing={4}>
        <Grid item xs={12} md={8} lg={9}>
          <Paper className={classes.paper}>
            <Title>Produtos do carrinho</Title>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell>Título</TableCell>
                  <TableCell>Valor</TableCell>
                  <TableCell>Estoque</TableCell>
                  <TableCell>Quantidade</TableCell>
                  <TableCell align="right" />
                  <TableCell align="right" />
                  <TableCell align="right" />
                </TableRow>
              </TableHead>
              <TableBody>
                {array_ids_produtos.map(row => (
                  <TableRow key={row.id}>
                    <TableCell>{row.title}</TableCell>
                    <TableCell align="right">
                      {formatter.format(row.price)}
                    </TableCell>
                    <TableCell>{row.quantity}</TableCell>
                    <TableCell>{row.qtde}</TableCell>
                    <TableCell>
                      <Button
                        size="small"
                        color="default"
                        variant="contained"
                        onClick={() => addProduct(row.id)}
                      >
                        +
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button
                        size="small"
                        color="default"
                        variant="contained"
                        onClick={() => removeProduct(row.id)}
                      >
                        -
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button
                        size="small"
                        color="secondary"
                        variant="contained"
                        onClick={() => removeProductAll(row.id)}
                      >
                        Remover
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Paper className={fixedHeightPaper}>
            <Title>Total</Title>
            <Typography component="p" variant="h4">
              {formatter.format(valor)}
            </Typography>
            <Typography
              color="textSecondary"
              className={classes.depositContext}
            >
              Valor total da compra
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
