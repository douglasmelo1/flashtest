import React, { useState } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

import products from "../Components/Produtos";
import { mainListItems } from "../Components/Menu";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      {new Date().getFullYear()}
    </Typography>
  );
}

export default function Home() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const formatter = new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL",
    minimumFractionDigits: 2
  });

  const handleDetail = id => {
    var idProduto = id.toString();

    var getStoreProducts = localStorage.getItem("produtos");

    var names = [];
    names = JSON.parse(getStoreProducts);

    if(names)
      names.push(idProduto);
    
      getStoreProducts = JSON.stringify(names);

    localStorage.setItem("produtos", getStoreProducts);

    alert("Adicionado ao carrinho!");

    window.location.href = '/carrinho';
    setCard(null);
  };

  const [cardnow, setCard] = useState(0);
  var getStoreProducts = localStorage.getItem("produtos");
  var badge = JSON.parse(getStoreProducts);
  
  const badge_count = (!badge) ? 0 : badge.length ; 

  return (
    <div className={classes.root}>
      <div>
        <CssBaseline />
        <AppBar
          position="absolute"
          className={clsx(classes.appBar, open && classes.appBarShift)}
        >
          <Toolbar className={classes.toolbar}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={clsx(
                classes.menuButton,
                open && classes.menuButtonHidden
              )}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className={classes.title}
            >
              AVEC - Lista de Produtos
            </Typography>
            <Link to="/carrinho">
              <IconButton className={classes.cor}>
                <Badge badgeContent={badge_count} color="secondary">
                  <ShoppingCartIcon />
                </Badge>
              </IconButton>
            </Link>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose)
          }}
          open={open}
        >
          <div className={classes.toolbarIcon}>
            <IconButton onClick={handleDrawerClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>{mainListItems}</List>
        </Drawer>
      </div>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={2}>
            {/* Chart */}
            <Grid item xs={12} md={12} lg={12}>
              <Paper className={classes.paper}>
                <Container className={classes.cardGrid} maxWidth="md">
                  {/* End hero unit */}
                  <Grid container spacing={4}>
                    {products.map(card =>
                      !cardnow ? (
                        <Grid item key={card.id} xs={12} sm={12} md={3}>
                          <Card className={classes.card}>
                            <CardMedia
                              className={classes.cardMedia}
                              image={card.picture}
                              title="Image title"
                            />
                            <CardContent className={classes.cardContent}>
                              <Typography>{card.title}</Typography>
                              <Typography
                                gutterBottom
                                variant="h5"  
                                component="h2"
                              >
                                {formatter.format(card.price)}
                              </Typography>
                            </CardContent>
                            <CardActions>
                              <Button
                                fullWidth
                                color="primary"
                                variant="contained"
                                onClick={() => setCard(card.id)}
                              >
                                Detalhes
                              </Button>
                            </CardActions>
                          </Card>
                        </Grid>
                      ) : cardnow == card.id ? (
                        <Grid item key={card.id} xs={12} sm={12} md={12}>
                          <Card className={classes.card}>
                            <CardContent className={classes.cardContent}>
                              <Grid item key={card.id} xs={4} sm={4} md={4}>
                                <CardMedia
                                  className={classes.cardMedia}
                                  image={card.picture}
                                  title="Image title"
                                />
                              </Grid>

                              <Typography className={classes.typographyCard}>
                                <b>Título: </b>
                                {card.title}
                              </Typography>
                              <Typography className={classes.typographyCard}>
                                <b>Memória: </b>
                                {card.memory}
                              </Typography>
                              <Typography className={classes.typographyCard}>
                                <b>Marca: </b>
                                {card.brand}
                              </Typography>
                              <Typography className={classes.typographyCard}>
                                <b>Chip: </b>
                                {card.chipType}
                              </Typography>
                              <Typography className={classes.typographyCard}>
                                <b>Estoque: </b>
                                {card.quantity}
                              </Typography>
                              <Typography className={classes.typographyCard}>
                                <b>Valor: </b>
                                {formatter.format(card.price)}
                              </Typography>
                              <Typography className={classes.typographyCard}>
                                <b>Descrição: </b>
                                {card.description}
                              </Typography>
                            </CardContent>
                            <CardActions>
                              <Button
                                fullWidth
                                color="primary"
                                variant="contained"
                                onClick={() => handleDetail(card.id)}
                              >
                                Adicionar ao carrinho
                              </Button>
                              <Button
                                fullWidth
                                color="secondary"
                                variant="contained"
                                onClick={() => setCard(null)}
                              >
                                Voltar
                              </Button>
                            </CardActions>
                          </Card>
                        </Grid>
                      ) : null
                    )}
                  </Grid>
                </Container>
              </Paper>
            </Grid>
          </Grid>
        </Container>
        <Copyright />
      </main>
    </div>
  );
}

const drawerWidth = 240;

// Folha de Estilo
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  cor: {
    color: "#fff"
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: "none"
  },
  title: {
    flexGrow: 1
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9)
    }
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  },
  typographyCard: {
    marginBottom: 5
  },
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "100%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6)
  }
}));
