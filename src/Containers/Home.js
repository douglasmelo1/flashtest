import React, { useState } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Title from "../Title";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import products from "../Components/Produtos";
import { mainListItems } from "../Components/Menu";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      {new Date().getFullYear()}
    </Typography>
  );
}

export default function Home() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [arr, setAr] = React.useState(null);
  const [cardnow, setCard] = useState(0);

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleOnChange = event => {
    setAr(event.target.value);
  };

  const formatter = new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL",
    minimumFractionDigits: 2
  });

  const handleDetail = arr => {
    if (!arr) {
      alert("Insira o número");
      return false;
    }
    var registers = [];

    fetch("http://localhost:3001/posts/"+arr, {
      method: 'GET',
      headers: {'Content-Type': 'application/json'}
  }).then(function(response) {
    if (response.status >= 400) {
      throw new Error("Bad response from server");
    }
    console.log('ENTREI1',response.status)  
    return response.json();
}).then(function(data) {
      
    console.log('ENTREI2',data)   
      registers = data;
      setCard(data);
  }).catch(function(err) {
      console.log('ERRO',err)
  });

    // var registers = [
    //   {
    //     id: 1,
    //     data: "23 fev 11:18",
    //     registro: "Informamos o vendedor da sua compra."
    //   },
    //   {
    //     id: 2,
    //     data: "25 fev 10:50",
    //     registro: "O vendedor está preparando o seu pacote."
    //   },
    //   {
    //     id: 3,
    //     data: "25 fev 13:22",
    //     registro: "O vendedor despachou o seu pacote."
    //   },
    //   { id: 4, data: "25 fev 14:39", registro: "Seu pacote está a caminho." },
    //   { id: 5, data: "27 fev 20:00", registro: "Entregamos o pacote." }
    // ];

    setAr(arr);
    

    // alert(arr)
  };

  var getStoreProducts = localStorage.getItem("produtos");
  var badge = JSON.parse(getStoreProducts);

  const badge_count = !badge ? 0 : badge.length;

  return (
    <div className={classes.root}>
      <div>
        <CssBaseline />
        <AppBar
          position="absolute"
          className={clsx(classes.appBar, open && classes.appBarShift)}
        >
          <Toolbar className={classes.toolbar}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={clsx(
                classes.menuButton,
                open && classes.menuButtonHidden
              )}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className={classes.title}
            >
              Transportadora Flashing
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose)
          }}
          open={open}
        >
          <div className={classes.toolbarIcon}>
            <IconButton onClick={handleDrawerClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>{mainListItems}</List>
        </Drawer>
      </div>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={2}>
            {/* Chart */}
            <Grid item xs={12} md={12} lg={12}>
              <Paper className={classes.paper}>
                <Container className={classes.cardGrid} maxWidth="md">
                  {/* End hero unit */}
                  <Grid container spacing={4}>
                    {!cardnow ? (
                      <Grid item key={1} xs={12} sm={12} md={6}>
                        <Card className={classes.card}>
                          <CardContent className={classes.cardContent}>
                            <TextField
                              required
                              id="standard-required"
                              label="Faça sua busca"
                              placeholder="EX.: AA100833276BR"
                              className={classes.textField}
                              margin="normal"
                              onChange={handleOnChange}
                            />
                          </CardContent>
                          <CardActions>
                            <Button
                              fullWidth
                              color="primary"
                              variant="contained"
                              onClick={() => handleDetail(arr)}
                            >
                              Buscar
                            </Button>
                          </CardActions>
                        </Card>
                      </Grid>
                    ) : cardnow ? (
                      <Grid item key={1} xs={12} sm={12} md={12}>
                        <Card className={classes.card}>
                          <CardContent className={classes.cardContent}>
                            <Paper className={classes.paper}>
                              <Title>Resultado do Rastreamento {arr}</Title>
                              <Table size="small">
                                <TableHead>
                                  <TableRow>
                                    <TableCell>Data</TableCell>
                                    <TableCell>Rastreio</TableCell>
                                  </TableRow>
                                </TableHead>
                                <TableBody>
                                  {cardnow.map((row,key) => (
                                    <TableRow key={row.id}>
                                      <TableCell>{row.data}</TableCell>
                                      <TableCell>{row.registro}</TableCell>
                                    </TableRow>
                                  ))}
                                </TableBody>
                              </Table>
                            </Paper>
                          </CardContent>
                          <CardActions>
                            <Button
                              fullWidth
                              color="secondary"
                              variant="contained"
                              onClick={() => setCard(null)}
                            >
                              Voltar
                            </Button>
                          </CardActions>
                        </Card>
                      </Grid>
                    ) : null}
                  </Grid>
                </Container>
              </Paper>
            </Grid>
          </Grid>
        </Container>
        <Copyright />
      </main>
    </div>
  );
}

const drawerWidth = 240;

// Folha de Estilo
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  cor: {
    color: "#fff"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: "none"
  },
  title: {
    flexGrow: 1
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9)
    }
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  },
  typographyCard: {
    marginBottom: 5
  },
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "100%",
    height: "70%",
    width: "70%"
  },
  cardContent: {
    flexGrow: 1
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6)
  }
}));
